/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './Apps/index';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
