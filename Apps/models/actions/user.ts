export interface IUserRequestState {
    type: String;
    idUser: number;
  }
  
  interface IResponse {
    user: any;
  }
  
  export interface IUserResponseState {
    type: String;
    response: IResponse;
  }

  //update user

  export interface IUpdateUserRequestState {
    type: String;
    idUser: number;
    first_name: string;
    last_name: string;
    email: string;
  }
