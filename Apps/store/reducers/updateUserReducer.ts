/* Login Reducer
 * handles login states in the app
 */
import createReducer from '../../lib/createReducer';
import * as types from '../actions/types';

import { IUserState } from '../../models/reducers/user';


import {
    IUserResponseState,
    IUpdateUserRequestState
  } from '../../models/actions/user';

const initialState: IUserState = {
  user: [],
};

// update user
export const updateUserReducer = createReducer(initialState, {
    [types.UPDATEUSER_REQUEST](state: IUserState, action: IUpdateUserRequestState) {
      return {
        ...state,
        idUser: action.idUser,
        first_name: action.first_name,
        last_name: action.last_name, 
        email: action.email
      };
    },
  
    [types.UPDATEUSER_RESPONSE](state: IUserState, action: IUserResponseState) {
      return {
        //...state,
        user:action.response
      };
    },
    [types.UPDATEUSER_FAILED](state: IUserState) {
      return {
        ...state,
      };
    },
    
  });