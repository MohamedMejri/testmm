/*
 * combines all th existing reducers
 */
import * as userReducer from './userReducer';
import * as updateUserReducer from './updateUserReducer';

export default Object.assign(userReducer, updateUserReducer);