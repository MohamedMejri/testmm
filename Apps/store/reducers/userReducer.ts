/* Login Reducer
 * handles login states in the app
 */
import createReducer from '../../lib/createReducer';
import * as types from '../actions/types';

import { IUserState } from '../../models/reducers/user';


import {
    IUserRequestState,
    IUserResponseState,
    IUpdateUserRequestState
  } from '../../models/actions/user';

const initialState: IUserState = {
  user: [],
};

export const getUserReducer = createReducer(initialState, {
  [types.GETUSER_REQUEST](state: IUserState, action: IUserRequestState) {
    return {
      ...state,
      idUser: action.idUser,
    };
  },

  [types.GETUSER_RESPONSE](state: IUserState, action: IUserResponseState) {
    return {
      //...state,
      user:action.response
    };
  },
  [types.GETUSER_FAILED](state: IUserState) {
    return {
      ...state,
    };
  },
  
});
// update user
export const updateUserReducer = createReducer(initialState, {
  [types.UPDATEUSER_REQUEST](state: IUserState, action: IUpdateUserRequestState) {
    return {
      ...state,
      idUser: action.idUser,
      first_name: action.first_name,
    };
  },

  [types.UPDATEUSER_RESPONSE](state: IUserState, action: IUserResponseState) {
    return {
      //...state,
      user:action.response
    };
  },
  [types.UPDATEUSER_FAILED](state: IUserState) {
    return {
      ...state,
    };
  },
  
});