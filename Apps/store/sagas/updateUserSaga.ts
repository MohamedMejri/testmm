
import { put, call } from 'redux-saga/effects';
// import { delay } from 'redux-saga';

import updateUser from '../../services/updateUser';
import * as userActions from '../actions/UserActions';

// Our worker Saga that update  user
export default function* updateUserWatcher(action:any) {
  //how to call api
  const response = yield call(updateUser, action.idUser, action.first_name, action.last_name, action.email);
  if (response.success) {
    yield put(userActions.updateUserResponse(response.data));
    yield put(userActions.getUserResponse(response));
  } else {
    yield put(userActions.updateUserFailed());
  }
}