/**
 *  Redux saga class init
 */
import { takeEvery, all, takeLatest } from 'redux-saga/effects';
import * as types from '../actions/types';
import getUserWatcher from './UserSaga';
import updateUserWatcher from './updateUserSaga';

const watchUser = takeEvery(types.GETUSER_REQUEST, getUserWatcher)
const watchUpdateUser = takeEvery(types.UPDATEUSER_REQUEST, updateUserWatcher)

export default function* watch() {
  yield all([watchUser, watchUpdateUser]);
}