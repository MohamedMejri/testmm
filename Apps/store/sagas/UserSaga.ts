
import { put, call } from 'redux-saga/effects';

import getUser from '../../services/getUser';
import * as userActions from '../actions/UserActions';


export default function* getUserWatcher(action:any) {
  const response = yield call(getUser, action.idUser);

  if (response.success) {
    yield put(userActions.getUserResponse(response.data));
  } else {
    yield put(userActions.getUserFailed());
  }
}

