// export action creators
import * as userActions from './UserActions';


export const ActionCreators = Object.assign(
  {},
  userActions,
);