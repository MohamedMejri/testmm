/*
 * Reducer actions related with login
 */
import * as types from './types';


export function requestgetUser(idUser: number) {
  return {
    type: types.GETUSER_REQUEST,
    idUser,
  };
}

export function getUserFailed() {
  return {
    type: types.GETUSER_FAILED,
  };
}

export function getUserResponse(response:any) {
  return {
    type: types.GETUSER_RESPONSE,
    response,
  };
}
// update user
export function updateUserRequest(idUser: number, first_name:string, last_name:string, email:string) {
  return {
    type: types.UPDATEUSER_REQUEST,
    idUser,
    first_name,
    last_name,
    email,
  };
}

export function updateUserFailed() {
  return {
    type: types.UPDATEUSER_FAILED,
  };
}

export function updateUserResponse(response:any) {
  return {
    type: types.UPDATEUSER_RESPONSE,
    response,
  };
}

