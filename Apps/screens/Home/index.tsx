import React, {useState, useEffect} from 'react';
import { View, Text, Button, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import * as userActions from '../../store/actions/UserActions';
import styles from './styles';
import { IUserState } from '../../models/reducers/user';


const HomeScreen: JSX.Element = ({navigation}) => {
  const dispatch = useDispatch();
  const user = useSelector((state: IUserState) => state.getUserReducer.user);
  const onGetUser = (id:number) => dispatch(userActions.requestgetUser(id));
 console.log('user', user)
  useEffect(()=>{
    onGetUser(2);
  },[])
  return (
    <View style={styles.container}>
    <Text style={{color:'black', fontSize:18}}>Bienvenu {user?.data?.first_name} {user?.data?.last_name}</Text>
    <Text style={{color:'black', fontSize:18, marginBottom:50}}>Email : {user?.data?.email}</Text>
    <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={() => navigation.navigate('Information')}>
              <Text style={styles.buttonTextStyle}>modifier vos informations</Text>
      </TouchableOpacity>
    </View>
  );
};

export default HomeScreen;