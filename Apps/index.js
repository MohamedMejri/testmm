/**
 * React Native App
 * Everything starts from the Entry-point
 */
import React from 'react';
import { ActivityIndicator } from 'react-native';

import { PersistGate } from 'redux-persist/es/integration/react';
import { Provider } from 'react-redux';
import AppNavigation from './Navigation/index' 
import configureStore from './store';

const { persistor, store } = configureStore();



const App = () => {
  return (
    <Provider store={store}>
        {/** 
    <PersistGate loading={<Text>Loading...</Text>} persistor={persistor}>
    */}
      <AppNavigation/>
      {/** 
    </PersistGate>
    */}
    </Provider>
  );
}

export default App;