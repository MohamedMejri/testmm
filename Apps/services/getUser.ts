
import axiosInstance from './axiosInstance';


export default function getUser(idUser: number) {
    return new Promise((resolve, reject) => {
      const result = {
        success: false,
        data: null,
      };
      axiosInstance
        .get(`/users/${idUser}`)
        .then(response => {
          result.success = true;
          result.data = response.data;
          resolve(result);
        })
        .catch(error => {
          result.success = false;
          result.data = error.response.data;
          //reject(result);
          resolve(result);
        });
    });
  
}
