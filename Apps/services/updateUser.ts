
import axiosInstance from './axiosInstance';


export default function updateUser(idUser: number, first_name:string, last_name:string, email:string) {
    return new Promise((resolve, reject) => {
      const result = {
        success: false,
        data: null,
      };
      axiosInstance
        .put(`/users/${idUser}`, {first_name: first_name, last_name: last_name, email: email})
        .then(response => {
          result.success = true;
          result.data = response.data;
          resolve(result);
        })
        .catch(error => {
          result.success = false;
          result.data = error.response.data;
          //reject(result);
          resolve(result);
        });
    });
  
}